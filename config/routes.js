export default [
  {
    path: '/platform',
    layout: false,
    routes: [
      {
        path: '/platform',
        routes: [{ name: '登录', path: '/platform/login', component: './platform/login' }],
      },
      { component: './404' },
    ],
  },
  { path: '/main', name: '首页', icon: 'smile', component: './main' },
  { path: '/', redirect: '/main' },
  { component: './404' },
];

import React, { useState, useRef } from 'react';
import { Alert, message, Tabs } from 'antd';
import { history, useModel, useIntl } from 'umi';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { ProFormText, LoginForm, ProFormCaptcha, ProForm } from '@ant-design/pro-form';
import {
  loginApi,
  getUserInfo,
  getSysName
} from '@/services/platform/login-api.js';
import { setSessionId, getSessionId } from '@/utils/cookies.js';
import md5 from '@/utils/md5.js';
import Footer from '@/components/Footer';
import styles from './index.less';
import dict from '@/utils/dict.js';
import { requestUrl,DEFAULT_CTX } from '@/utils/request';

const LoginMessage = ({ content }) => (
  <Alert style={{ marginBottom: 24 }} message={content} type="error" showIcon />
);

const Login = () => {
  const [userLoginState, setUserLoginState] = useState({});
  const [showVc,setShowVc] = useState(false);
  const [sysName,setSysName] = useState('');
  const [type, setType] = useState('account');
  const { initialState, setInitialState } = useModel('@@initialState');
  const intl = useIntl();
  const sessionId = getSessionId();
  const catpchaRef = useRef();

  const fetchUserInfo = async () => {
    const userInfo = await getUserInfo();
    userInfo.data.access = 'admin';

    if (userInfo) {
      sessionStorage.setItem('userInfo', JSON.stringify(userInfo.data));
      await setInitialState((s) => {
        return { ...s, currentUser: userInfo.data };
      });
    }
  };

  const initSysName = () =>{
    getSysName().then(res=>{
      if(res.ok){
        setSysName(res.data);
      }
    })
  }
  initSysName();
  const handleSubmit = async (values) => {
    try {
      // 登录
      let reqData = values;
      reqData.password = md5(reqData.password);
      const msg = await loginApi({ ...reqData, type });

      if (msg.ok) {
        setShowVc(false);
        setSessionId(msg.data);
        const defaultLoginSuccessMessage = '登录成功！';
        message.success(defaultLoginSuccessMessage);
        await fetchUserInfo();
        dict.init(); //初始化字典
        /** 此方法会跳转到 redirect 参数所在的位置 */

        if (!history) return;
        const { query } = history.location;
        const { redirect } = query;
        history.push(redirect || '/main');
        return;
      } else {
        if(msg.code===-2){
          setShowVc(true);
          if (msg.attr.JSESSIONID) {
            setSessionId(msg.attr.JSESSIONID);
          }
          refreshCaptcha();
        }
      }

      setUserLoginState(msg);
    } catch (error) {
      console.log(error);
    }
  };

  const { status, type: loginType } = userLoginState;

  const refreshCaptcha = () => {
    if(catpchaRef.current){
      catpchaRef.current.src = `${requestUrl}${DEFAULT_CTX}/ep/sys/verification-codes?_t_=`+new Date().getMilliseconds();
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <LoginForm
          logo={<img alt="logo" src="/logo.png" style={{ marginTop: '5px' }} />}
          title={sysName}
          className={styles.login_form}
          initialValues={{ autoLogin: true }}
          onFinish={async (values) => {
            await handleSubmit(values);
          }}
        >
          <Tabs activeKey={type} onChange={setType}>
            <Tabs.TabPane key="account" tab={'登录'} />
          </Tabs>

          {status === 'error' && <LoginMessage content={'错误的用户名和密码'} />}

          {type === 'account' && (
            <>
              <ProFormText
                name="username"
                fieldProps={{
                  size: 'large',
                  prefix: <UserOutlined className={styles.prefixIcon} />,
                }}
                placeholder={'请输入用户名'}
                rules={[{ required: true, message: '用户名是必填项！' }]}
              />

              <ProFormText.Password
                name="password"
                fieldProps={{
                  size: 'large',
                  prefix: <LockOutlined className={styles.prefixIcon} />,
                }}
                placeholder={'请输入密码'}
                rules={[{ required: true, message: '密码是必填项！' }]}
              />

              {showVc && (
                <>
                  <div className={styles.captchaWrap}>
                    <ProFormText
                      name="verificationCode"
                      class="ProFormText"
                      fieldProps={{
                        size: 'large',
                        prefix: <UserOutlined className={styles.prefixIcon} />,
                      }}
                      placeholder={'请输入验证码'}
                      rules={[{ required: true, message: '验证码是必填项！' }]}
                    ></ProFormText>
                  </div>
                  <img
                    className={styles.captchaImg}
                    ref={catpchaRef}
                    src={`${requestUrl}/${DEFAULT_CTX}/ep/sys/verification-codes?_ht_=${getSessionId()}`}
                    onClick={refreshCaptcha}
                  />
                </>
              )}
            </>
          )}
        </LoginForm>
      </div>

      <Footer />
    </div>
  );
};

export default Login;

import React from 'react';
import { Table, Form, Row, Col, Input, Button, Tree, Popconfirm, Modal, message } from 'antd';
import { SearchOutlined, PlusOutlined } from '@ant-design/icons';
import { PageContainer } from '@ant-design/pro-layout';
import style from '@/less/global.less';
import base from '@/utils/base.js';
import {
  queryList,
  deleteEntityById,
  saveEntity,
  queryRoleMenuList,
} from '@/services/system/role-api';
import * as menuApi from '@/services/system/menu-api';

class RolePage extends React.Component {
  constructor(props) {
    super(props);
    this.editForm = React.createRef();
  }

  componentDidMount() {
    const { pagination } = this.state;
    menuApi.queryList().then((res) => {
      if (res.ok) {
        this.setState({
          roleMenuList: res.data,
        });
      }
    });
    this.refreshTable({ pagination });
  }

  componentWillUnmount() {
    this.setState({});
  }

  state = {
    condition: {},
    data: [],
    pagination: base.createPagination(this),
    loading: false,
    editType: 1, // 编辑框的操作类型， 1 - 新增； 2- 修改
    roleMenuList: [], //角色树
    selectedMenus: [], //被选择的角色树
  };

  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      align: 'center',
      width: '100px',
      render: (text) => <a>{text}</a>,
    },
    { title: '角色名称', dataIndex: 'roleName', align: 'center', width: '120px' },
    { title: '角色标签', dataIndex: 'roleTag', align: 'center', width: '120px' },
    {
      title: '操作',
      width: '160px',
      align: 'left',
      fixed: 'right',
      render: (row) => {
        return (
          <div>
            <Button
              className={style.op_btn}
              type="primary"
              size="small"
              onClick={() => this.editRow(row)}
            >
              修改
            </Button>
            <Popconfirm title="是否确认删除?" onConfirm={() => this.deleteRow(row)}>
              <Button className={style.op_btn} type="danger" size="small">
                删除
              </Button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  handleTableChange = (pagination, filters, sorter) => {
    this.refreshTable({
      pagination,
      filters,
      sorter,
    });
  };

  /**
   * 数据查询
   * @param condition 填写的查询条件
   */
  queryList = (condition) => {
    this.setState({ condition: condition });
    this.refreshTable({ pagination: { current: 1 } });
  };

  /**
   * 刷新数据列表
   */
  refreshTable = (params = {}) => {
    this.setState({ loading: true });
    queryList({
      ...this.state.condition,
      pagination: { ...this.state.pagination, ...params.pagination },
    }).then((res) => {
      if (res.ok) {
        this.setState({
          loading: false,
          data: res.data.list,
          pagination: {
            ...this.state.pagination,
            ...params.pagination,
            total: res.data.total,
          },
          isModalVisible: false, // 显示新增或修改弹窗
        });
      }
    });
  };

  /**
   * 删除指定表格行
   */
  deleteRow = (row) => {
    deleteEntityById(row.id).then((res) => {
      if (res.ok) {
        message.success('删除成功').then();
        this.refreshTable();
      }
    });
  };

  /**
   * 添加或修改记录
   */
  saveData = (formData) => {
    Reflect.set(formData, 'selectedMenus', this.state.selectedMenus.join(','));
    this.setState({ loading: true });
    saveEntity(formData).then((res) => {
      this.setState({ loading: false });
      if (res.ok) {
        message.success('保存成功').then();
        this.editForm.current.resetFields();
        this.refreshTable();
      }
    });
  };

  /**
   * 编辑指定表格行
   */
  editRow = async (row) => {
    queryRoleMenuList(row.id).then((res) => {
      if (res.ok) {
        this.setState({ editType: 2, isModalVisible: true, selectedMenus: res.data }, () => {
          this.editForm.current.setFieldsValue(row);
        });
      }
    });
  };

  /**
   * 打开新增或修改modal
   */
  showModal = () => {
    this.setState({ editType: 1, isModalVisible: true });
  };

  /**
   * 点击modal取消按钮
   */
  hideModel = () => {
    this.setState({ isModalVisible: false, selectedMenus: [] });
    this.editForm.current.resetFields();
  };

  onCheckMenuTree = (checkedKeysValue) => {
    this.setState({ selectedMenus: checkedKeysValue.checked });
  };

  render() {
    const { data, pagination, loading, isModalVisible, editType, selectedMenus, roleMenuList } =
      this.state;
    return (
      <PageContainer>
        <Form className={style.table_search} autoComplete="off" onFinish={this.queryList}>
          <Row gutter={24}>
            <Col span={6}>
              <Form.Item label="角色名称" name="roleName">
                <Input placeholder="请输入角色名称" allowClear />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item style={{ textAlign: 'left' }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  size="middle"
                  className={style.op_btn}
                  icon={<SearchOutlined />}
                >
                  查询
                </Button>

                <Button
                  type="primary"
                  size="middle"
                  className={style.op_btn}
                  icon={<PlusOutlined />}
                  onClick={this.showModal}
                >
                  新增
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>

        <Table
          columns={this.columns}
          bordered={true}
          rowKey={(record) => record.id}
          dataSource={data}
          pagination={pagination}
          loading={loading}
          onChange={this.handleTableChange}
          size="middle"
        />

        <Modal
          title={editType === 1 ? '新增' : '修改'}
          visible={isModalVisible}
          closable={false}
          footer={null}
        >
          <Form
            onFinish={this.saveData}
            autoComplete="off"
            labelCol={{ span: 4 }}
            ref={this.editForm}
          >
            <Form.Item name="id" hidden={true}>
              <Input />
            </Form.Item>
            <Form.Item
              label="角色名称"
              name="roleName"
              rules={[{ required: true, message: '请输入角色名称' }]}
            >
              <Input placeholder="请输入角色名称" allowClear />
            </Form.Item>
            <Form.Item label="角色标签" name="roleTag">
              <Input placeholder="请输入角色标签" allowClear />
            </Form.Item>
            <Form.Item label="角色权限">
              <Tree
                checkable
                checkStrictly
                onExpand={false}
                checkedKeys={selectedMenus}
                treeData={roleMenuList}
                onCheck={this.onCheckMenuTree}
                fieldNames={{ title: 'name', key: 'id', children: 'children' }}
              />
            </Form.Item>
            <Form.Item style={{ textAlign: 'center' }}>
              <Button className={style.op_btn} size="middle" onClick={this.hideModel}>
                取消
              </Button>
              <Button className={style.op_btn} type="primary" htmlType="submit" size="middle">
                保存
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </PageContainer>
    );
  }
}

export default RolePage;

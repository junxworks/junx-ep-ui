import React from 'react';
import {
  Table,
  Form,
  Row,
  Col,
  Input,
  Button,
  Select,
  DatePicker,
  Popconfirm,
  Modal,
  message,
  TreeSelect,
  Radio,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { PageContainer } from '@ant-design/pro-layout';
import moment from 'moment';
import style from '@/less/global.less';
import dict from '@/utils/dict.js';
import { queryList, deleteEntityById, saveEntity, getEntityById } from '@/services/system/org-api';

class OrgPage extends React.Component {
  constructor(props) {
    super(props);
    this.editForm = React.createRef();
  }

  componentDidMount() {
    this.refreshTable();
  }

  componentWillUnmount() {
    this.setState({});
  }

  state = {
    condition: {},
    data: [],
    loading: false,
    editType: 1, // 编辑框的操作类型， 1 - 新增； 2- 修改
    menuType: '0',
    rowData: {},
  };

  columns = [
    { title: '组织名称', dataIndex: 'orgName', width: '200px', align: 'left' },
    { title: '组织编码', dataIndex: 'orgNo', width: '100px', align: 'left' },
    {
      title: '组织类型',
      dataIndex: 'orgType',
      width: '100px',
      align: 'center',
      render: (orgType) => dict.translate('orgType', orgType),
    },
    { title: '备注', dataIndex: 'remark', width: '200px', align: 'left' },
    {
      title: '操作',
      width: '160px',
      align: 'left',
      fixed: 'right',
      render: (row) => {
        return (
          <div>
            <Button
              className={style.op_btn}
              type="primary"
              size="small"
              onClick={() => this.editRow(row)}
            >
              修改
            </Button>
            <Popconfirm title="是否确认删除?" onConfirm={() => this.deleteRow(row)}>
              <Button className={style.op_btn} type="danger" size="small">
                删除
              </Button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  /**
   * 数据查询
   * @param condition 填写的查询条件
   */
  queryList = (condition) => {
    condition.createTime = condition.createTime
      ? moment(condition.createTime).format('YYYY-MM-DD')
      : '';
    this.setState({ condition: condition });
    this.refreshTable();
  };

  /**
   * 刷新数据列表
   */
  refreshTable = () => {
    this.setState({ loading: true });
    queryList({
      ...this.state.condition,
    }).then((res) => {
      if (res.ok) {
        this.setState({
          loading: false,
          data: res.data,
          isModalVisible: false, // 显示新增或修改弹窗
        });
      }
    });
  };

  /**
   * 删除指定表格行
   */
  deleteRow = (row) => {
    deleteEntityById(row.id).then((res) => {
      if (res.ok) {
        message.success('删除成功').then();
        this.refreshTable();
      }
    });
  };

  /**
   * 添加或修改记录
   */
  saveData = (formData) => {
    this.setState({ loading: true });
    saveEntity(formData).then((res) => {
      this.setState({ loading: false });
      if (res.ok) {
        message.success('保存成功').then();
        this.editForm.current.resetFields();
        this.refreshTable();
      }
    });
  };

  /**
   * 编辑指定表格行
   */
  editRow = (row) => {
    getEntityById(row.id).then((res) => {
      let rowData = res.data;
      Reflect.set(rowData, 'type', String(rowData.type));
      this.setState({ editType: 2, isModalVisible: true, menuType: rowData.type, rowData }, () => {
        this.editForm.current.setFieldsValue(rowData);
      });
    });
  };

  /**
   * 打开新增或修改modal
   */
  showModal = () => {
    this.setState({ editType: 1, isModalVisible: true });
  };

  /**
   * 点击modal取消按钮
   */
  hideModel = () => {
    this.setState({ isModalVisible: false, menuType: '0' });
    this.editForm.current.resetFields();
  };

  /**
   * 菜单类型选项切换
   * @param type
   */
  handleTypeChange = (event) => {
    let type = event.target.value;
    this.setState({ menuType: type });
  };

  openIconsPage = () => {
    window.open('https://ant.design/components/icon-cn/');
  };

  render() {
    const { data, loading, isModalVisible, editType, menuType } = this.state;
    const { Search } = Input;

    return (
      <PageContainer>
        <Form className={style.table_search} autoComplete="off" onFinish={this.allocate}>
          <Row gutter={24}>
            <Col span={6}>
              <Form.Item style={{ textAlign: 'left' }}>
                <Button
                  type="primary"
                  size="middle"
                  className={style.op_btn}
                  icon={<PlusOutlined />}
                  onClick={this.showModal}
                >
                  新增
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>

        <Table
          columns={this.columns}
          bordered={true}
          rowKey={(record) => record.id}
          dataSource={data}
          pagination={false}
          loading={loading}
          size="middle"
        />

        <Modal
          title={editType === 1 ? '新增' : '修改'}
          visible={isModalVisible}
          closable={false}
          footer={null}
          width={600}
        >
          <Form
            onFinish={this.saveData}
            autoComplete="off"
            labelCol={{ span: 4 }}
            ref={this.editForm}
          >
            <Form.Item name="id" hidden={true}>
              <Input />
            </Form.Item>
            <Form.Item
              label="组织编码"
              name="orgNo"
              rules={[{ required: true, message: '请输入名编码' }]}
            >
              <Input placeholder="请输入编码" allowClear disabled={editType === 2} />
            </Form.Item>
            <Form.Item
              label="组织名称"
              name="orgName"
              rules={[{ required: true, message: '请输入名称' }]}
            >
              <Input placeholder="请输入名称" allowClear />
            </Form.Item>
            <Form.Item label="上级组织" name="parentNo">
              <TreeSelect
                style={{ width: '100%' }}
                dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                treeData={data}
                fieldNames={{ label: 'orgName', value: 'orgNo', children: 'children' }}
              />
            </Form.Item>
            <Form.Item
              label="组织类型"
              name="orgType"
              rules={[{ required: true, message: '请选择类型' }]}
            >
              <Select placeholder="请选择类型" allowClear>
                {dict.selectOptions('orgType')}
              </Select>
            </Form.Item>
            <Form.Item label="备注" name="remark">
              <Input placeholder="请填写备注" allowClear />
            </Form.Item>
            <Form.Item style={{ textAlign: 'center' }}>
              <Button className={style.op_btn} size="middle" onClick={this.hideModel}>
                取消
              </Button>
              <Button className={style.op_btn} type="primary" htmlType="submit" size="middle">
                保存
              </Button>
            </Form.Item>
          </Form>
        </Modal>
      </PageContainer>
    );
  }
}

export default OrgPage;

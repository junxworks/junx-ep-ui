import React from 'react';
import { Form, Input, Button, Modal, message } from 'antd';
import style from '@/less/global.less';
import { getEntityById, savePassword } from '@/services/system/user-api';
import createMD5 from '@/utils/md5';
import { getUserInfo } from '@/services/platform/login-api.js';

class ResetPwdModel extends React.Component {
  beforeSave = undefined;

  constructor(props) {
    super(props);
    this.pwdForm = React.createRef();
  }

  componentWillUnmount() {
    this.setState({});
  }

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.userId !== this.props.userId) {
      getEntityById(this.props.userId).then((res) => {
        if (res.ok) {
          let u = res.data;
          let user = {
            id: u.id,
            name: u.name,
            username: u.username,
            pass: '',
          };
          if (this.pwdForm.current) {
            this.pwdForm.current.setFieldsValue(user);
          }
        }
      });
    } else {
      let u = JSON.parse(sessionStorage.getItem('userInfo'));
      let user = { id: u.id, name: u.name, username: u.username, pass: '' };
      if (this.pwdForm.current) this.pwdForm.current.setFieldsValue(user);
    }
  }

  savePwd = async (formData) => {
    if (formData.pass === '123456') {
      message.error('请不要设置成系统默认密码').then();
      return;
    }
    if (this.props.beforeSave) {
      if (!this.props.beforeSave(formData)) {
        return;
      }
    }
    Reflect.set(formData, 'pass', createMD5(formData.pass));
    savePassword(formData).then((res) => {
      if (res.ok) {
        getUserInfo().then((res) => {
          sessionStorage.setItem('userInfo', JSON.stringify(res.data));
          sessionStorage.removeItem('userShouldResetPwd');
          message.success('操作成功').then();
        });
        if (this.props.onDone) {
          this.props.onDone(formData);
        }
      }
    });
  };

  /**
   * 点击modal取消按钮
   */
  handleCancel = (item) => {
    if (this.props.onCancel) {
      this.props.onCancel(item);
    }
  };

  render() {
    const { ...modelProps } = this.props;
    return (
      <Modal
        title="重置密码"
        {...modelProps}
        closable={false}
        keyboard={false}
        maskClosable={false}
        footer={null}
      >
        <Form
          onFinish={this.savePwd}
          autoComplete="off"
          ref={this.pwdForm}
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 24 }}
        >
          <Form.Item name="id" hidden={true}>
            <Input />
          </Form.Item>
          <Form.Item label="姓名" name="name">
            <Input bordered={false} readOnly />
          </Form.Item>
          <Form.Item label="账号" name="username">
            <Input bordered={false} readOnly />
          </Form.Item>
          <Form.Item label="密码" name="pass" rules={[{ required: true, message: '请输入密码' }]}>
            <Input.Password placeholder="请输入密码" allowClear />
          </Form.Item>
          <Form.Item style={{ textAlign: 'center' }}>
            {modelProps.showCancelBtn && (
              <Button className={style.op_btn} size="middle" onClick={this.handleCancel}>
                取消
              </Button>
            )}
            <Button className={style.op_btn} type="primary" htmlType="submit" size="middle">
              保存
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

export default ResetPwdModel;

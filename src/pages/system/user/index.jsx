import React from 'react';
import {
  Table,
  Form,
  Row,
  Col,
  Input,
  Button,
  Select,
  Popconfirm,
  Modal,
  message,
  TreeSelect,
  Transfer,
} from 'antd';
import { SearchOutlined, PlusOutlined } from '@ant-design/icons';
import { PageContainer } from '@ant-design/pro-layout';
import style from '@/less/global.less';
import dict from '@/utils/dict.js';
import base from '@/utils/base.js';
import {
  queryList,
  lockUserById,
  saveEntity,
  getEntityById,
  getRolesByUserId,
} from '@/services/system/user-api';
import * as orgApi from '@/services/system/org-api';
import * as roleApi from '@/services/system/role-api';
import ResetPwdModel from '@/pages/system/user/reset-pwd-model';

class UserPage extends React.Component {
  constructor(props) {
    super(props);
    this.editForm = React.createRef();
  }

  componentDidMount() {
    const { pagination } = this.state;
    this.refreshTable({ pagination });
    orgApi.queryList().then((res) => {
      if (res.ok) {
        this.setState({ orgData: res.data });
      }
    });
    roleApi.queryList({ pagination: { current: 1, pageSize: 100000 } }).then((res) => {
      if (res.ok) {
        let roleOptions = [];
        res.data.list.forEach((d) => {
          roleOptions.push(<Select.Option key={d.id}>{d.roleName}</Select.Option>);
        });
        this.setState({ roleOptions: roleOptions });
      }
    });
  }

  componentWillUnmount() {
    this.setState({});
  }

  state = {
    condition: {},
    data: [],
    pagination: base.createPagination(this),
    loading: false,
    editType: 1, // 编辑框的操作类型， 1 - 新增； 2- 修改
    statusOptions: dict.selectOptions('userStatus'),
    orgData: [],
    roleOptions: [],
    roleData: [],
    targetRoles: [],
    isResetPwdModalVisible: false,
    userIdToResetPwd: 0,
  };

  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      align: 'center',
      width: '80px',
      render: (text) => <a>{text}</a>,
    },
    { title: '账号', dataIndex: 'username', align: 'center', width: '120px' },
    { title: '姓名', dataIndex: 'name', align: 'center', width: '120px' },
    {
      title: '状态',
      dataIndex: 'status',
      align: 'center',
      width: '80px',
      render: (status) => dict.translate('userStatus', status),
    },
    { title: '所属组织', dataIndex: 'orgName', width: '120px', align: 'center' },
    { title: '角色', dataIndex: 'roleName', align: 'left', ellipsis: true, width: '200px' },
    { title: '手机号', dataIndex: 'mobile', width: '120px', align: 'center' },
    { title: '邮箱', dataIndex: 'email', width: '200px', align: 'center' },
    {
      title: '操作',
      width: '160px',
      align: 'left',
      fixed: 'right',
      render: (row) => {
        return (
          <div>
            <Button
              className={style.op_btn}
              type="primary"
              size="small"
              onClick={() => this.editRow(row)}
            >
              修改
            </Button>
            <Button
              className={style.op_btn}
              type="primary"
              size="small"
              onClick={() => this.resetPwd(row)}
            >
              重置密码
            </Button>
            <Popconfirm
              title={`是否确认冻结“${row.name}”`}
              onConfirm={() => this.lockUser(row)}
              disabled={row.status !== 0}
            >
              <Button
                className={style.op_btn}
                type="danger"
                size="small"
                disabled={row.status !== 0}
              >
                冻结
              </Button>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  handleTableChange = (pagination, filters, sorter) => {
    this.refreshTable({
      pagination,
      filters,
      sorter,
    });
  };

  /**
   * 数据查询
   * @param condition 填写的查询条件
   */
  queryList = (condition) => {
    condition.roles = condition.roles ? condition.roles.join(',') : '';
    this.setState({ condition: condition });
    this.refreshTable({ pagination: { current: 1 } });
  };

  /**
   * 刷新数据列表
   */
  refreshTable = (params = {}) => {
    this.setState({ loading: true });
    queryList({
      ...this.state.condition,
      pagination: { ...this.state.pagination, ...params.pagination },
    }).then((res) => {
      if (res.ok) {
        this.setState({
          loading: false,
          data: res.data.list,
          pagination: {
            ...this.state.pagination,
            ...params.pagination,
            total: res.data.total,
          },
          isModalVisible: false, // 显示新增或修改弹窗
        });
      }
    });
  };

  /**
   * 删除指定表格行
   */
  lockUser = (row) => {
    lockUserById(row.id).then((res) => {
      if (res.ok) {
        message.success('操作成功').then();
        this.refreshTable();
      }
    });
  };

  /**
   * 编辑指定表格行
   */
  editRow = async (row) => {
    await this.initRoles(row.id);
    getEntityById(row.id).then((res) => {
      let rowData = res.data;
      Reflect.set(rowData, 'status', String(rowData.status));
      this.setState({ editType: 2, isModalVisible: true }, () => {
        this.editForm.current.setFieldsValue(rowData);
      });
    });
  };

  /**
   * 重置密码
   * @param row
   * @returns {Promise<void>}
   */
  resetPwd = (row) => {
    this.setState(
      {
        isResetPwdModalVisible: true,
        userIdToResetPwd: row.id,
      },
      () => {},
    );
  };

  /**
   * 添加或修改记录
   */
  saveData = (formData) => {
    this.setState({ loading: true });
    let roles = [];
    this.state.targetRoles.forEach((r) => {
      roles.push({ value: r });
    });
    Reflect.set(formData, 'roles', roles);
    saveEntity(formData).then((res) => {
      this.setState({ loading: false });
      if (res.ok) {
        message.success('保存成功').then();
        this.editForm.current.resetFields();
        this.refreshTable();
      }
    });
  };

  /**
   * 打开新增或修改modal
   */
  showModal = () => {
    this.initRoles(0).then((res) => {
      this.setState({ editType: 1, isModalVisible: true });
    });
  };

  initRoles = async (userId) => {
    await getRolesByUserId(userId).then((res) => {
      if (res.ok) {
        let roles = res.data;
        let rs = [];
        roles.forEach((r) => {
          rs.push({ key: String(r.id), title: r.roleName, description: r.roleName });
        });
        let target = [];
        if (res.attr.checked) {
          res.attr.checked.forEach((c) => {
            target.push(String(c));
          });
        }
        this.setState({ roleData: rs, targetRoles: target });
      }
    });
  };

  /**
   * 点击modal取消按钮
   */
  hideModel = () => {
    this.editForm.current.resetFields();
    this.setState({ isModalVisible: false, targetRoles: [] });
  };

  handleChange = (nextTargetKeys) => {
    this.setState({ targetRoles: nextTargetKeys });
  };

  render() {
    const {
      data,
      orgData,
      roleData,
      targetRoles,
      roleOptions,
      pagination,
      loading,
      isModalVisible,
      editType,
      statusOptions,
      isResetPwdModalVisible,
      userIdToResetPwd,
    } = this.state;

    return (
      <PageContainer>
        <Form
          className={style.table_search}
          autoComplete="off"
          onFinish={this.queryList}
          labelCol={{ span: 6 }}
        >
          <Row gutter={30}>
            <Col span={6}>
              <Form.Item label="姓名或手机号" name="query">
                <Input allowClear />
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item label="人员状态" name="status">
                <Select placeholder="全部" allowClear>
                  {statusOptions}
                </Select>
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item label="所属组织" name="orgNo">
                <TreeSelect
                  style={{ width: '100%' }}
                  dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                  treeData={orgData}
                  fieldNames={{ label: 'orgName', value: 'orgNo', children: 'children' }}
                  allowClear
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={30}>
            <Col span={6}>
              <Form.Item label="角色" name="roles">
                <Select mode="multiple" style={{ width: '100%' }} placeholder="请选择角色">
                  {roleOptions}
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item style={{ textAlign: 'left' }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  size="middle"
                  className={style.op_btn}
                  icon={<SearchOutlined />}
                >
                  查询
                </Button>

                <Button
                  type="primary"
                  size="middle"
                  className={style.op_btn}
                  icon={<PlusOutlined />}
                  onClick={this.showModal}
                >
                  新增
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>

        <Table
          columns={this.columns}
          bordered={true}
          rowKey={(record) => record.id}
          dataSource={data}
          pagination={pagination}
          loading={loading}
          onChange={this.handleTableChange}
          size="middle"
          scroll={{ scrollToFirstRowOnChange: true, x: '100%', y: false }}
        />

        <Modal
          title={editType === 1 ? '新增' : '修改'}
          visible={isModalVisible}
          closable={false}
          footer={null}
          width={800}
        >
          <Form
            onFinish={this.saveData}
            autoComplete="off"
            ref={this.editForm}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 24 }}
          >
            <Form.Item name="id" hidden={true}>
              <Input />
            </Form.Item>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  label="姓名"
                  name="name"
                  rules={[{ required: true, message: '请输入姓名' }]}
                >
                  <Input placeholder="请输入姓名" allowClear />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="所属组织" name="orgNo">
                  <TreeSelect
                    style={{ width: '100%' }}
                    dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                    treeData={orgData}
                    fieldNames={{ label: 'orgName', value: 'orgNo', children: 'children' }}
                    allowClear
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item
                  label="账号"
                  name="username"
                  rules={[{ required: true, message: '请输入账号' }]}
                >
                  <Input placeholder="请输入账号" allowClear />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="密码">
                  <Input defaultValue="初始密码为123456" disabled />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={24}>
              <Col span={12}>
                <Form.Item label="手机号" name="mobile">
                  <Input placeholder="请输入手机号" allowClear />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="身份证号" name="idCard">
                  <Input placeholder="请输入身份证号" allowClear />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item label="邮箱" name="email">
                  <Input placeholder="请输入邮箱" allowClear />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  label="状态"
                  name="status"
                  initialValue={'0'}
                  rules={[{ required: true, message: '请选择状态' }]}
                >
                  <Select placeholder="全部" allowClear>
                    {statusOptions}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Form.Item label="角色选择" labelCol={{ span: 3 }}>
              <Transfer
                titles={['系统角色', '已有角色']}
                dataSource={roleData}
                showSearch
                targetKeys={targetRoles}
                onChange={this.handleChange}
                render={(item) => `${item.title}`}
                listStyle={{
                  width: 300,
                  height: 300,
                }}
              />
            </Form.Item>
            <Form.Item style={{ textAlign: 'center' }}>
              <Button className={style.op_btn} size="middle" onClick={this.hideModel}>
                取消
              </Button>
              <Button className={style.op_btn} type="primary" htmlType="submit" size="middle">
                保存
              </Button>
            </Form.Item>
          </Form>
        </Modal>
        <ResetPwdModel
          visible={isResetPwdModalVisible}
          userId={userIdToResetPwd}
          showCancelBtn={true}
          onCancel={() => this.setState({ isResetPwdModalVisible: false })}
          onDone={() => this.setState({ isResetPwdModalVisible: false })}
          beforeSave={(formData) => {
            return true; //true继续保存 false中断保存
          }}
        />
      </PageContainer>
    );
  }
}

export default UserPage;

import React from 'react';
import { Table, Form, Row, Col, Input, Button, DatePicker, Drawer } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { PageContainer } from '@ant-design/pro-layout';
import moment from 'moment';
import style from '@/less/global.less';
import base from '@/utils/base.js';
import { queryList } from '@/services/system/syslog-api';

class SysLogPage extends React.Component {
  constructor(props) {
    super(props);
    this.editForm = React.createRef();
  }

  componentDidMount() {
    const { pagination } = this.state;
    this.refreshTable({ pagination });
  }

  componentWillUnmount() {
    this.setState({});
  }

  state = {
    condition: {},
    data: [],
    pagination: base.createPagination(this),
    loading: false,
  };

  columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      align: 'center',
      width: '80px',
      render: (text) => <a>{text}</a>,
    },
    { title: '操作用户名', dataIndex: 'name', align: 'center', width: '120px' },
    { title: '操作名称', dataIndex: 'operation', width: '200px', ellipsis: true, align: 'left' },
    { title: '请求地址', dataIndex: 'url', width: '300px', ellipsis: true, align: 'left' },
    { title: '请求IP', dataIndex: 'ip', width: '150px', align: 'right' },
    { title: '请求数据', dataIndex: 'data', width: '300px', ellipsis: true, align: 'left' },
    { title: '请求方法', dataIndex: 'method', width: '200px', ellipsis: true, align: 'left' },
    { title: '执行耗时(ms)', dataIndex: 'cost', width: '120px', align: 'right' },
    { title: '操作时间', align: 'center', dataIndex: 'createTime', width: '200px' },
    {
      title: '操作',
      width: '100px',
      align: 'left',
      fixed: 'right',
      render: (row) => {
        return (
          <div>
            <Button
              className={style.op_btn}
              type="primary"
              size="small"
              onClick={() => this.editRow(row)}
            >
              查看
            </Button>
          </div>
        );
      },
    },
  ];

  handleTableChange = (pagination, filters, sorter) => {
    this.refreshTable({
      pagination,
      filters,
      sorter,
    });
  };

  /**
   * 数据查询
   * @param condition 填写的查询条件
   */
  queryList = (condition) => {
    condition.createDate = condition.createDate
      ? moment(condition.createDate).format('YYYY-MM-DD')
      : '';
    this.setState({ condition: condition });
    this.refreshTable({ pagination: { current: 1 } });
  };

  /**
   * 刷新数据列表
   */
  refreshTable = (params = {}) => {
    this.setState({ loading: true });
    queryList({
      ...this.state.condition,
      pagination: { ...this.state.pagination, ...params.pagination },
    }).then((res) => {
      if (res.ok) {
        this.setState({
          loading: false,
          data: res.data.list,
          pagination: {
            ...this.state.pagination,
            ...params.pagination,
            total: res.data.total,
          },
          isModalVisible: false, // 显示新增或修改弹窗
        });
      }
    });
  };

  /**
   * 编辑指定表格行
   */
  editRow = (row) => {
    this.setState({ isModalVisible: true }, () => {
      Reflect.set(row, 'createTime', moment(row.createTime).format('YYYY-MM-DD HH:mm:ss'));
      this.editForm.current.setFieldsValue(row);
    });
  };

  /**
   * 打开新增或修改modal
   */
  showModal = () => {
    this.setState({ isModalVisible: true });
  };

  /**
   * 点击modal取消按钮
   */
  hideModel = () => {
    this.setState({ isModalVisible: false });
    this.editForm.current.resetFields();
  };

  render() {
    const { data, pagination, loading, isModalVisible, editType, sexOptions } = this.state;

    return (
      <PageContainer>
        <Form className={style.table_search} autoComplete="off" onFinish={this.queryList}>
          <Row gutter={24}>
            <Col span={6}>
              <Form.Item label="操作员姓名" name="name">
                <Input allowClear />
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item label="操作名称：" name="opName">
                <Input allowClear />
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item label="操作日期" name="createDate">
                <DatePicker className={style.modal_date} format={'YYYY-MM-DD'} />
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item style={{ textAlign: 'left' }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  size="middle"
                  className={style.op_btn}
                  icon={<SearchOutlined />}
                >
                  查询
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>

        <Table
          columns={this.columns}
          bordered={true}
          rowKey={(record) => record.id}
          dataSource={data}
          pagination={pagination}
          loading={loading}
          onChange={this.handleTableChange}
          size="middle"
          scroll={{ scrollToFirstRowOnChange: true, x: '100%', y: false }}
        />

        <Drawer title="操作日志" visible={isModalVisible} closable={false} size="large">
          <Form autoComplete="off" ref={this.editForm}>
            <Row gutter={24}>
              <Col span={9}>
                <Form.Item label="操作员" name="name">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={9}>
                <Form.Item label="操作时间" name="createTime">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label="耗时(ms)" name="cost">
                  <Input bordered={false} />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col span={12}>
                <Form.Item label="操作名称" name="operation">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="请求IP地址" name="ip">
                  <Input />
                </Form.Item>
              </Col>
            </Row>
            <Form.Item label="请求地址" name="url">
              <Input />
            </Form.Item>
            <Form.Item label="请求方法" name="method">
              <Input />
            </Form.Item>
            <Form.Item label="请求数据" name="data">
              <Input.TextArea autoSize={true} />
            </Form.Item>

            <Form.Item style={{ textAlign: 'center' }}>
              <Button className={style.op_btn} size="middle" onClick={this.hideModel}>
                取消
              </Button>
            </Form.Item>
          </Form>
        </Drawer>
      </PageContainer>
    );
  }
}

export default SysLogPage;

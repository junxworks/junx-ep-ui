import * as React from 'react';

import {watermark} from '@/utils/waterMark'

export default class WaterMark extends React.Component {
  constructor(props) {
    super(props);
    this.container = null;
  }

  componentDidMount () {
    // const content = localStorage.getItem('jkWaterContent') || '';

  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.markContent !== this.props.markContent) {
      const { markContent,style, ...options } = this.props;
      console.log(markContent)
      watermark({ container: this.container, content:markContent, ...options, });
    }
  }

  render () {
    const style = { position: 'relative', ...this.props.style, };
    return ( <div ref={(el) => this.container = el} id="watermark" style={style}> {this.props.children} </div> );
  }}

import React from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import Home from './home';

const Main = () => {
  return (
    <PageContainer>
      {/*<Image preview={false} src={mainPng} />*/}
      {/*<Image preview={false} src={mainlogoPng} width={150} />*/}
      <Home />
    </PageContainer>
  );
};

export default Main;

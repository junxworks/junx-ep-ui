import { PageLoading } from '@ant-design/pro-layout';
import { history, Link } from 'umi';
import RightContent from '@/components/RightContent';
import Footer from '@/components/Footer';
import { getUserInfo } from '@/services/platform/login-api.js';
import { queryCurrentUserMenus } from '@/services/platform/user-api.js';
import { setAsyncRoutes } from '@/utils/async-routes.js';
import { getSessionId } from '@/utils/cookies.js';
import { BookOutlined, LinkOutlined } from '@ant-design/icons';

const isDev = process.env.NODE_ENV === 'development';
const loginPath = '/platform/login';
/** 获取用户信息比较慢的时候会展示一个 loading */

export const initialStateConfig = {
  loading: <PageLoading />,
};
/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 * */

let asyncRoutes = [];

export async function getInitialState() {
  const fetchUserInfo = async () => {
    try {
      let msgData = '';
      if (getSessionId()) {
        const msg = await getUserInfo();
        msgData = msg.data;
      }
      return msgData;
    } catch (error) {
      history.push(loginPath);
    }

    return undefined;
  }; // 如果是登录页面，不执行

  if (history.location.pathname !== loginPath) {
    const currentUser = await fetchUserInfo();
    return {
      fetchUserInfo,
      currentUser,
      settings: {},
    };
  }

  return {
    fetchUserInfo,
    settings: {},
  };
} // ProLayout 支持的api https://procomponents.ant.design/components/layout

// 路由白名单，无需登录
const whiteList = ['/anon/public-demo'];

export const layout = ({ initialState }) => {
  let _collapse = JSON.parse(localStorage.getItem('selfDefineCollapse'));

  return {
    rightContentRender: () => <RightContent />,

    disableContentMargin: false,

    // waterMarkProps: {
    //   content: initialState?.currentUser?.name,
    // },
    // footerRender: () => <Footer />,

    onPageChange: () => {
      const { location } = history; // 如果没有登录，重定向到 login

      if (!initialState?.currentUser && location.pathname !== loginPath && location.pathname.indexOf("/sso/")===-1) {
        history.push(loginPath);
      }
    },
    links: isDev ? [] : [],
    menuHeaderRender: undefined,
    breakpoint: false,
    defaultCollapsed: _collapse === true ? true : false,
    onCollapse: collapsed => {
      localStorage.setItem('selfDefineCollapse', collapsed);
    },
    menuItemRender: (menuItemProps, defaultDom) => {
      if (menuItemProps.isUrl || !menuItemProps.path) {
        return defaultDom;
      }
      // 支持二级菜单显示icon
      return (
        <Link to={menuItemProps.path}>
          {menuItemProps.pro_layout_parentKeys &&
            menuItemProps.pro_layout_parentKeys.length > 0 &&
            menuItemProps.icon}
          {defaultDom}
        </Link>
      );
    },

    // 动态添加路由菜单
    menu: {
      locale: false,
      params: initialState,
      request: async () => {
        // 登录页面，不渲染top和sideMenu
        let menuList = [{ path: '/platform/login', headerRender: false, menuRender: false }];

        // 白名单，无需登录页面，不渲染top和sideMenu
        let whitePageList = [];
        whiteList.forEach( item => {
          let _route = { path: `${item}`, headerRender: false, menuRender: false };
          whitePageList.push(_route);
        });

        // 权限路由
        if (initialState.currentUser && initialState.currentUser.username) {
          const menuData = await queryCurrentUserMenus();
          menuList = setAsyncRoutes(menuData);
        }

        menuList = [ ...menuList, ...whitePageList ];
        return menuList;
      },
    },

    // 自定义 403 页面
    // unAccessible: <div>unAccessible</div>,
    // 增加一个 loading 的状态
    // childrenRender: (children) => {
    //   if (initialState.loading) return <PageLoading />;
    //   return children;
    // },

    ...initialState?.settings,
  };
};

import { GithubOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-layout';

const Footer = () => {
    const defaultMessage = 'Junxworks出品';
    const currentYear = new Date().getFullYear();
    return (
        <DefaultFooter
            copyright={`${currentYear} ${defaultMessage}`}
            links={[
                {
                    key: 'Junxworks',
                    title: 'Junx-EP',
                    href: 'https://www.junxworks.cn',
                    blankTarget: true,
                }
            ]}
        />
    );
};

export default Footer;

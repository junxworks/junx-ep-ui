import React, { useCallback, useState } from 'react';
import { LogoutOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Menu, Spin } from 'antd';
import { history, useModel } from 'umi';
import { stringify } from 'querystring';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';
import { logout } from '@/services/platform/login-api.js';
import { removeSessionId } from '@/utils/cookies.js';
import ResetPwdModel from '@/pages/system/user/reset-pwd-model';

/**
 * 退出登录，并且将当前的 url 保存
 */
const loginOut = async () => {
  await logout();
  removeSessionId();
  sessionStorage.removeItem('userInfo');
  history.replace({ pathname: '/user/login' });
};

const AvatarDropdown = ({ menu }) => {
  const { initialState, setInitialState } = useModel('@@initialState');
  const [showModal, setShowModal] = useState(false);
  const [shown, setShown] = useState(false);
  const [showCancelBtn, setShowCancelBtn] = useState(true);

  const userShouldResetPwd = sessionStorage.getItem('userShouldResetPwd');
  if (!shown && userShouldResetPwd && userShouldResetPwd==='1') {
    setShown(true);
    setShowCancelBtn(false);
    setShowModal(true);
  }

  const onMenuClick = useCallback(
    (event) => {
      const { key } = event;
      if (key === 'logout') {
        setInitialState((s) => ({ ...s, currentUser: undefined }));
        loginOut();
        return;
      } else if (key === 'settings') {
        setShowCancelBtn(true);
        setShowModal(true);
      } else history.push(`/account/${key}`);
    },
    [setInitialState],
  );
  const loading = (
    <span className={`${styles.action} ${styles.account}`}>
      <Spin
        size="small"
        style={{
          marginLeft: 8,
          marginRight: 8,
        }}
      />
    </span>
  );

  if (!initialState) {
    return loading;
  }

  const { currentUser } = initialState;

  if (!currentUser || !currentUser.name) {
    return loading;
  }

  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
      {menu && (
        <Menu.Item key="center">
          <UserOutlined />
          个人中心
        </Menu.Item>
      )}

      <Menu.Item key="settings">
        <SettingOutlined />
        修改密码
      </Menu.Item>

      <Menu.Item key="logout">
        <LogoutOutlined />
        退出登录
      </Menu.Item>
    </Menu>
  );
  return (
    <>
      <HeaderDropdown overlay={menuHeaderDropdown}>
        <span className={`${styles.action} ${styles.account}`}>
          <Avatar size="small" className={styles.avatar} src={currentUser.avatar} alt="avatar" />
          <span className={`${styles.name} anticon`}>{currentUser.name}</span>
        </span>
      </HeaderDropdown>

      <ResetPwdModel
        visible={showModal}
        userId={currentUser.id}
        showCancelBtn={showCancelBtn}
        onCancel={() => setShowModal(false)}
        onDone={() => setShowModal(false)}
        beforeSave={(formData) => {
          return true; //true继续保存 false中断保存
        }}
      />
    </>
  );
};

export default AvatarDropdown;

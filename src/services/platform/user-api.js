import request, {DEFAULT_CTX} from '@/utils/request.js';

/**
 * 查询当前用户的所有菜单
 * @returns {Promise<any>}
 */
export async function queryCurrentUserMenus() {
  return request.get(`${DEFAULT_CTX}/ep/sys/menus/menus-bar`);
}

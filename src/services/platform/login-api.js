import request,{DEFAULT_CTX} from '@/utils/request.js';

/**
 * 登录
 */
export const loginApi = async (reqData) =>
  request.post(`${DEFAULT_CTX}/ep/sys/login`, { data: reqData });

/**
 * 获取登录用户信息
 */
export const getUserInfo = async () => request.get(`${DEFAULT_CTX}/ep/sys/users/current-user`);

/**
 * 登出
 */
export const logout = () => request.post(`${DEFAULT_CTX}/ep/sys/logout`);

export const getSysName = async () => request.get(`${DEFAULT_CTX}/ep/sys/system-name`);

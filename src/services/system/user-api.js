import request, { initRequestParams,DEFAULT_CTX } from '@/utils/request.js';

/**
 * 根据条件查询列表
 * @param params
 * @returns {Promise<any>}
 */
export async function queryList(params) {
  return request.get(`${DEFAULT_CTX}/ep/sys/users`, { params: initRequestParams(params) });
}

export async function lockUserById(id) {
  return request.put(`${DEFAULT_CTX}/ep/sys/users/status`, { data: { id: id, status: 1 } });
}

/**
 * 根据ID查询对象
 * @param id
 * @returns {Promise<any>}
 */
export async function getEntityById(id) {
  return request.get(`${DEFAULT_CTX}/ep/sys/users/` + id);
}

/**
 * 根据人员ID查询角色相关信息
 * @param id
 * @returns {Promise<any>}
 */
export async function getRolesByUserId(id) {
  return request.get(`${DEFAULT_CTX}/ep/sys/users/${id}/roles`);
}

/**
 * 保存对象
 * @param params
 * @returns {Promise<any>}
 */
export async function saveEntity(params) {
  return request.post(`${DEFAULT_CTX}/ep/sys/users`, { data: params });
}

/**
 * 保存密码
 * @param params
 * @returns {Promise<any>}
 */
export async function savePassword(params) {
  return request.put(`${DEFAULT_CTX}/ep/sys/users/pass`, { data: params });
}

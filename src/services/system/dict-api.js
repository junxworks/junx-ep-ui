import request, { initRequestParams,DEFAULT_CTX } from '@/utils/request.js';

/**
 * 根据条件查询列表
 * @param params
 * @returns {Promise<any>}
 */
export async function queryList(params) {
  return request.get(`${DEFAULT_CTX}/ep/sys/dictionaries`, { params: initRequestParams(params) });
}

/**
 * 删除对象
 * @param id
 * @returns {Promise<any>}
 */
export async function deleteEntityById(id) {
  return request.delete(`${DEFAULT_CTX}/ep/sys/dictionaries/` + id);
}

/**
 * 根据ID查询对象
 * @param id
 * @returns {Promise<any>}
 */
export async function getEntityById(id) {
  return request.get(`${DEFAULT_CTX}/ep/sys/dictionaries/` + id);
}

/**
 * 保存对象
 * @param params
 * @returns {Promise<any>}
 */
export async function saveEntity(params) {
  return request.post(`${DEFAULT_CTX}/ep/sys/dictionaries`, { data: params });
}

import request, { initRequestParams,DEFAULT_CTX } from '@/utils/request.js';

/**
 * 根据条件查询列表
 * @param params
 * @returns {Promise<any>}
 */
export async function queryList(params) {
  return request.get(`${DEFAULT_CTX}/ep/sys/roles`, { params: initRequestParams(params) });
}

export async function queryRoleMenuList(id) {
  return request.get(`${DEFAULT_CTX}/ep/sys/roles/${id}/checked-menus`);
}

/**
 * 删除对象
 * @param id
 * @returns {Promise<any>}
 */
export async function deleteEntityById(id) {
  return request.delete(`${DEFAULT_CTX}/ep/sys/roles/` + id);
}

/**
 * 保存对象
 * @param params
 * @returns {Promise<any>}
 */
export async function saveEntity(params) {
  return request.post(`${DEFAULT_CTX}/ep/sys/roles/`, { data: params });
}

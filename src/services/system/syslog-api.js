import request, { initRequestParams,DEFAULT_CTX } from '@/utils/request.js';

/**
 * 根据条件查询列表
 * @param params
 * @returns {Promise<any>}
 */
export async function queryList(params) {
  return request.get(`${DEFAULT_CTX}/ep/sys/system-logs`, { params: initRequestParams(params) });
}

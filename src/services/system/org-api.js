import request,{DEFAULT_CTX} from '@/utils/request.js';

/**
 * 根据条件查询列表
 * @param params
 * @returns {Promise<any>}
 */
export async function queryList() {
  return request.get(`${DEFAULT_CTX}/ep/sys/orgs/tree`);
}

/**
 * 删除对象
 * @param id
 * @returns {Promise<any>}
 */
export async function deleteEntityById(id) {
  return request.delete(`${DEFAULT_CTX}/ep/sys/orgs/` + id);
}

/**
 * 根据ID查询对象
 * @param id
 * @returns {Promise<any>}
 */
export async function getEntityById(id) {
  return request.get(`${DEFAULT_CTX}/ep/sys/orgs/` + id);
}

/**
 * 保存对象
 * @param params
 * @returns {Promise<any>}
 */
export async function saveEntity(params) {
  if (params.id) {
    return request.put(`${DEFAULT_CTX}/ep/sys/orgs`, { data: params });
  } else {
    return request.post(`${DEFAULT_CTX}/ep/sys/orgs`, { data: params });
  }
}

import moment from 'moment';

/**
 * 基础工具类
 * @type {{isNull: ((function(*=): boolean)|*)}}
 */
class Base {
  /**
   * 判断对象是否为空
   * @param obj
   * @returns {boolean}
   */
  isNull = (obj) => {
    if (obj === 0) {
      return false;
    }
    return obj == null || typeof obj == 'undefined' || obj === '' || obj === 'null';
  };

  /**
   * 时间戳转成日期YYYY-MM-DD
   * @param timestamp
   * @returns {string}
   */
  timestamp2DateStr = (timestamp) => {
    if (!timestamp) return '-';
    return moment(timestamp).format('YYYY-MM-DD');
  };

  /**
   * 构造默认分页插件对象
   * @param pageComponent 当前component,如 base.createPagination(this)
   * @returns {{current: number, total: number, pageSizeOptions: string[], size: string, showTotal: (function(): string), hideOnSinglePage: boolean, pageSize: number, showQuickJumper: boolean, showSizeChanger: boolean}}
   */
  createPagination = (pageComponent) => {
    return {
      current: 1,
      pageSize: 10,
      total: 0,
      size: 'small',
      showSizeChanger: true,
      hideOnSinglePage: false,
      showQuickJumper: true,
      pageSizeOptions: ['10', '30', '50', '100'],
      // 总数
      showTotal: function () {
        return `总共有 ${pageComponent.state.pagination.total} 条数据`;
      },
    };
  };

  paginationParam = {
    current: 1,
    pageSize: 10,
    size: 'small',
    showSizeChanger: true,
    hideOnSinglePage: false,
    showQuickJumper: true,
    pageSizeOptions: ['10', '30', '50', '100'],
    // 总数
    showTotal: (total, range) => `总共有 ` + total + `条数据`,
  };

  /**
   * 根据文件id生成图片访问接口
   * @param id
   * @returns {string}
   */
  imageUrl = (fileId) => {
    let ht = sessionStorage.getItem('JSESSIONID');
    let reqUrl='';
    if(fileId.indexOf(".")>-1){
      reqUrl = `${process.env.apiUrl}/yrsc-bp/ep/files/${fileId}?_ht_=${ht}`;
    }else{
      reqUrl = `${process.env.apiUrl}/yrsc-bp/ep/files/${fileId}/images?_ht_=${ht}`;
    }
    return reqUrl;
  };

  fileUploadUrl = () => {
    return `${process.env.apiUrl}/yrsc-bp/ep/files`;
  }

  /**
   * 数字格式化千分位
   */
  numFormat = (num, unit = 2) => num ? (num.toFixed(unit) + '').replace(/\d{1,3}(?=(\d{3})+(\.\d*)?$)/g, '$&,') : '0.00';
}

const base = new Base();
export default base;

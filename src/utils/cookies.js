const sessionId = 'JSESSIONID';

export const getSessionId = () => sessionStorage.getItem(sessionId);

export const setSessionId = (value) => sessionStorage.setItem(sessionId, value);

export const removeSessionId = () => sessionStorage.removeItem(sessionId);

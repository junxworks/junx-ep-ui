import request,{DEFAULT_CTX} from '@/utils/request.js';
import base from '@/utils/base.js';
import { Select, Radio } from 'antd';

const DICT_PREFIX = '$global_dictionary_';

const objToStrMap = (obj) => {
  let map = new Map();
  for (let k of Object.keys(obj)) {
    map.set(k, obj[k]);
  }
  return map;
};

class Dict {
  /**
   * 通过父类型与数据字典值取对应的数据字典翻译，
   * 如果dataCode为空，则返回parentCode对应的数据字典
   *
   * @param parentCode
   * @param exclude 需要排除的regex表达式
   * @returns
   */
  // return new Promise((resolve, reject) => {
  //   if ( right ) {
  //     resolve(correctData)
  //   }
  //   else {
  //     reject(errorData)
  //   }
  // });

  init() {
    request.get(`${DEFAULT_CTX}/ep/sys/dictionaries/total`).then((res) => {
      let data = objToStrMap(res.data);
      data.forEach((v, k, m) => {
        let d = JSON.stringify(v);
        let key = DICT_PREFIX + k;
        sessionStorage.setItem(key, d);
      });
    });
  }

  dictionaryMap(parentCode, exclude) {
    let key = DICT_PREFIX + parentCode;
    let data = sessionStorage.getItem(key);
    return this.handleDictData(data, exclude);
  }

  /**
   * 生成下拉菜单的选项
   * @param parentCode
   * @param options
   * @returns {any[]}
   */
  selectOptions(parentCode, options) {
    return this.generateSomething(parentCode, options, (v, k, m) => {
      return (
        <Select.Option key={k} value={k}>
          {v}
        </Select.Option>
      );
    });
  }

  /**
   * 生成单选按钮
   * @param parentCode
   * @param options
   * @returns {*[]}
   */
  radioButtons(parentCode, options) {
    return this.generateSomething(parentCode, options, (v, k, m) => {
      return (
        <Radio.Button key={k} value={k}>
          {v}
        </Radio.Button>
      );
    });
  }

  /**
   * 生成下拉菜单的选项
   * @param parentCode
   * @param options
   * @param genFunction
   * @returns {any[]}
   */
  generateSomething(parentCode, options, genFunction) {
    let exclude = '';
    if (options) {
      exclude = options.exclude;
    }
    let map = this.dictionaryMap(parentCode, exclude);
    let arr = new Array();
    if (map != null) {
      map.forEach((v, k, m) => {
        arr.push(genFunction(v, k, m));
      });
    }
    return arr;
  }

  handleDictData(data, exclude) {
    if (!base.isNull(data)) {
      let array = JSON.parse(data);
      let map = new Map();
      for (let i = 0; i < array.length; i++) {
        let item = array[i];
        let k = String(item['dataCode']);
        let v = String(item['dataLabel']);
        if (!base.isNull(exclude)) {
          if (!exclude.test(k)) {
            map.set(k, v);
          }
        } else {
          map.set(k, v);
        }
      }
      return map;
    }
    return null;
  }

  /**
   * 将数据值翻译成对应的字典描述
   * @param parentCode
   * @param dataCode
   * @returns {string|null|V}
   */
  translate(parentCode, dataCode) {
    if (base.isNull(dataCode)) {
      return '';
    }
    let dict = this.dictionaryMap(String(parentCode));
    if (dict != null) {
      return String(dict.get(String(dataCode)));
    }
    return null;
  }

  /**
   * 将多个数据值翻译成对应的字典描述，用逗号隔开
   * parentCode:类型
   * dataCodes: 数据编码，用逗号隔开
   */
  translateMulti(parentCode, dataCodes) {
    if (base.isNull(dataCodes)) {
      return '';
    }
    let res = '';
    let strs = dataCodes.split(',');
    for (let i = 0; i < strs.length; i++) {
      if (i > 0) {
        res = res + ',' + this.translate(parentCode, strs[i]);
      } else {
        res = this.translate(parentCode, strs[i]);
      }
    }
    return res;
  }
}

const dict = new Dict();

export default dict;

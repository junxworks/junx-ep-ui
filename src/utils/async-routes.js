import icons from '@/utils/icons.js';

const handleMenuItems = (item) => {
  if (item.path) {
    let route = {
      path: item.path,
      name: item.name,
    };
    if (item.icon) {
      route.icon = icons.createIcon(item.icon);
    }
    if (item.component) {
      route.component = item.component;
    }
    if (item.children) {
      let childRoutes = [];
      item.children.forEach((cItem) => {
        if (cItem.path) {
          childRoutes.push(handleMenuItems(cItem));
        }
      });
      route.routes = childRoutes;
    }
    return route;
  }
  return null;
};

/**
 * 处理从api获取的菜单列表数据
 * @param  menuData api获取的菜单列表数据
 * @return route需要的格式数据
 */
const hideList = [];
export const setAsyncRoutes = (menuData) => {
  if (menuData.code !== 0) return [];

  let resultList = [];
  resultList.push({
    path: '/main',
    name: '首页',
    icon: icons.createIcon('SketchOutlined'),
    component: './main',
  });
  let menuList = menuData.data;
  // 添加api获取的动态路由
  menuList.forEach((item) => {
    let menuItem = handleMenuItems(item);
    if (menuItem) {
      // 隐藏设置了不再列表栏中显示的菜单
      menuItem.routes.forEach( item => {
        if ( hideList.indexOf(item.name) > -1 ) {
          item.hideInMenu = true;
        }
      });
      resultList.push(menuItem);
    }
  });
  return resultList;
};

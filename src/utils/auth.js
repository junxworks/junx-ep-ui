/**
 * 权限认证
 */
class Auth {
  /**
   * 判断当前登录人员是否有指定权限
   * @param authorization 需要校验的权限
   * @returns boolean
   */
  authorize(authorization) {
    let userInfo = sessionStorage.getItem('userInfo');
    let user = JSON.parse(userInfo);
    let auths = user.authorizations;
    for (let i in auths) {
      if (auths[i] === authorization) {
        return true;
      }
    }
    return false;
  }

  /**
   * 判断当前登录人员是否有指定角色
   * @param roleTag
   * @returns {boolean}
   */
  hasRole(roleTag) {
    let userInfo = sessionStorage.getItem('userInfo');
    let user = JSON.parse(userInfo);
    let roles = user.roles;
    for (let i in roles) {
      if (roles[i] === roleTag) {
        return true;
      }
    }
    return false;
  }

  isDataOwner(dataOwnerId){
    let userInfo = sessionStorage.getItem('userInfo');
    let user = JSON.parse(userInfo);
    return dataOwnerId===user.id;
  }

  isAdmin(){
    let userInfo = sessionStorage.getItem('userInfo');
    let user = JSON.parse(userInfo);
    return -1==user.id;
  }
}

const auth = new Auth();
export default auth;

import {extend} from 'umi-request';
import {notification} from 'antd';
import {getSessionId} from '@/utils/cookies.js';

export const requestUrl = process.env.apiUrl;

export const DEFAULT_CTX = '/demo';

const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};

/**
 * 异常处理程序
 */
const errorHandler = (error) => {
  const {response} = error;
  if (response && response.status) {
    const errorText = codeMessage[response.status] || response.statusText;
    const {status, url} = response;
    notification.error({
      message: `请求错误 ${status}: ${url}`,
      description: errorText,
    });
  } else if (!response) {
    notification.error({
      description: '您的网络发生异常，无法连接服务器',
      message: '网络异常',
    });
  }
  return response;
};

const initInterceptors = (req) => {
  req.interceptors.request.use(async (url, options) => {
    let sessionId = getSessionId();
    if (sessionId) {
      const headers = {_ht_: sessionId};
      return {
        url: url,
        options: {...options, headers: headers},
      };
    } else {
      return {
        url: url,
        options: options,
      };
    }
  });
  req.interceptors.response.use(async (response, options) => {
    const {url, status} = response;
    if (url.indexOf('/verification-codes') !== -1) {
      return response;
    }
    if (url.indexOf('/ep/fs/files/') !== -1) {
      if (status !== 200) {
        notification.error({
          message: `下载异常 : ${url}`,
        });
      } else {
        return await response.clone().blob();
      }
      return null;
    }

    let res = await response.clone().json();
    if (!res.ok) {
      if (res.code === 4) { //用户需要重置密码
          res.ok=true;
          sessionStorage.setItem('userShouldResetPwd', '1');
          return res;
      } else {
        notification.error({
          message: `请求错误`,
          description: res.msg,
        });
      }
    }
    return res;
  });
}

const request = extend({
  errorHandler,
  prefix: requestUrl,
  timeout: 1000 * 60,
  credentials: 'include',
});

initInterceptors(request);

export default request;

export const initRequestParams = (params) => ({
  pageSize: params.pagination.pageSize,
  pageNo: params.pagination.current,
  ...params,
});

export async function proTableRequest(params, url) {
  const parm = params ? {...params, pageNo: params.current} : {pageSize: 10, pageNo: 1};
  const res = await request.get(url, {params: parm});
  if (!res.ok) {
    return Promise.resolve({
      success: false,
      ok: false,
    });
  }
  const result = {
    data: res.data.list || [],
    // success 请返回 true，不然table 会停止解析数据，即使有数据
    ok: res.ok,
    success: res.ok,
    // 不传会使用 data 的长度，如果是分页一定要传
    total: parseInt(res.data.total),
    pageSize: res.data.pageSize,
    current: res.data.pageNum,
    attr: res.attr,
  };
  return Promise.resolve(result);

}

export const createRequest = (extUrl) => {
  return extend({
    errorHandler,
    prefix: extUrl,
    timeout: 1000 * 60,
    credentials: 'include',
  });
}

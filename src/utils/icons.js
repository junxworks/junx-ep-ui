import * as allIcons from '@ant-design/icons';
import React from 'react';
import base from '@/utils/base.js';

class Icons {
  createIcon = (iconName) => {
    if (base.isNull(iconName)) {
      return null;
    }
    let _iconName = iconName.trim();
    const newIcon = allIcons[_iconName] || allIcons[''.concat(_iconName, 'Outlined')];
    if (newIcon) {
      try {
        return React.createElement(newIcon);
      } catch (error) {
        console.log(error);
      }
    }
    return null;
  };
}

const icons = new Icons();

export default icons;
